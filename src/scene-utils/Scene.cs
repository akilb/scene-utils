﻿using System.Collections.Generic;

namespace Scene.Utils {
    public class Scene {
        public Scene() {
            Dependencies = new List<string>();

            XResolution = 800;
            YResolution = 600;
            HaltSamplesPerPixel = -1;

            Film = Film.FlexImage;
            PixelFilter = PixelFilter.Mitchell;
            Accelerator = Accelerator.KdTree;
            Renderer = Renderer.Sampler;
            Camera = Camera.Perspective;
            Sampler = Sampler.Random;
            VolumeIntegrator = VolumeIntegrator.Emission;
            SurfaceIntegrator = SurfaceIntegrator.BiDirectional;
        }

        public IList<string> Dependencies           { get; set; }

        public string Path                          { get; set; }
        public int XResolution                      { get; set; }
        public int YResolution                      { get; set; }
        public int HaltSamplesPerPixel              { get; set; }

        public Film Film                            { get; set; }
        public Camera Camera                        { get; set; }
        public Sampler Sampler                      { get; set; }
        public Renderer Renderer                    { get; set; }
        public PixelFilter PixelFilter              { get; set; }
        public Accelerator Accelerator              { get; set; }
        public VolumeIntegrator VolumeIntegrator    { get; set; }
        public SurfaceIntegrator SurfaceIntegrator  { get; set; }
    }
}