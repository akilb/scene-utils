﻿namespace Scene.Utils.Packagers {
    public interface IScenePackager {
        void Package(string scenePath, string outputPackagePath);
        void Unpackage(string packagePath, string targetDirectoryPath);
    }
}
