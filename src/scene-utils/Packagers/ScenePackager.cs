﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.IO;
using System.Linq;
using Ionic.Zip;
using Scene.Utils.Scanners;

namespace Scene.Utils.Packagers {
    public class ScenePackager : IScenePackager {
        private readonly ISceneScanner _scanner;
        private readonly IFileHelper _fileHelper;
        private readonly IZipFileFactory _zipFactory;

        public ScenePackager()
            : this(new SceneScanner(),
                   new FileHelper(),
                   new ZipFileFactory()) {
        }

        public ScenePackager(ISceneScanner scanner,
                             IFileHelper fileHelper,
                             IZipFileFactory zipFactory) {
            Contract.Requires(scanner != null);
            Contract.Requires(fileHelper != null);
            Contract.Requires(zipFactory != null);

            _scanner = scanner;
            _fileHelper = fileHelper;
            _zipFactory = zipFactory;
        }

        public void Package(string scenePath, string outputPackagePath) {
            string sceneDirectory = Path.GetDirectoryName(scenePath);

            var scene = _scanner.Scan(scenePath);

            var sceneFiles = scene.Dependencies.Select(d => {
                var relativeDependency = d.Replace('/', '\\').Replace(sceneDirectory, "");
                if (relativeDependency[0] == '\\') {
                    relativeDependency = relativeDependency.Remove(0, 1);
                }

                return relativeDependency;
            }).ToList();
            sceneFiles.Add(Path.GetFileName(scenePath));

            using (var zip = _zipFactory.CreateZip()) {
                foreach (var file in sceneFiles) {
                    var filePath = Path.Combine(sceneDirectory, file);
                    if (!_fileHelper.FileExists(filePath)) {
                        // TODO: Fire an event that add failed...
                        continue;
                    }

                    zip.AddFile(filePath,
                                Path.GetDirectoryName(file));
                }

                zip.Save(outputPackagePath);
            }
        }

        public void Unpackage(string packagePath, string targetDirectoryPath) {
            using (var zip = _zipFactory.ReadZip(packagePath)) {
                foreach (var entry in zip) {
                    System.Console.WriteLine("Extracting:\t{0}", entry.FileName);
                    zip.Extract(entry,
                                targetDirectoryPath,
                                ExtractExistingFileAction.OverwriteSilently);
                }
            }
        }

        #region ZipFile Helpers

        public interface IZipFileFactory {
            IZipFile CreateZip();
            IZipFile ReadZip(string path);
        }

        public interface IZipFile : IEnumerable<ZipEntry>, IDisposable {
            void AddFile(string filePath, string directoryPathInArchive);
            void Extract(ZipEntry entry,
                         string targetDirectory,
                         ExtractExistingFileAction extractExistingFile);
            void Save(string path);
        }

        private class ZipFileFactory : IZipFileFactory {
            public IZipFile CreateZip() {
                return new ZipFileWrapper(new ZipFile());
            }

            public IZipFile ReadZip(string path) {
                return new ZipFileWrapper(ZipFile.Read(path));
            }
        }

        private class ZipFileWrapper : IZipFile {
            private readonly ZipFile _zip;

            public ZipFileWrapper(ZipFile zip) {
                _zip = zip;
            }

            public void AddFile(string filePath, string directoryPathInArchive) {
                _zip.AddFile(filePath, directoryPathInArchive);
            }

            public void Extract(ZipEntry entry,
                                string targetDirectory,
                                ExtractExistingFileAction extractExistingFile) {
                entry.Extract(targetDirectory, extractExistingFile);
            }

            public void Save(string path) {
                _zip.Save(path);
            }

            public IEnumerator<ZipEntry> GetEnumerator() {
                return _zip.GetEnumerator();
            }

            IEnumerator IEnumerable.GetEnumerator() {
                return _zip.GetEnumerator();
            }

            public void Dispose() {
                _zip.Dispose();
            }
        }

        #endregion

        #region FileHelper

        public interface IFileHelper {
            bool FileExists(string filePath);
        }

        private class FileHelper : IFileHelper {
            public bool FileExists(string filePath) {
                return File.Exists(filePath);
            }
        }

        #endregion
    }
}
