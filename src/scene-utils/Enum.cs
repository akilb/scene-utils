﻿namespace Scene.Utils {
    public enum Renderer {
        Sampler = 0,
        HybridSampler = 1,
        HybridSppm = 2
    }

    public enum Camera {
        Perspective = 0,
        Environment = 1,
        Orthographic = 2,
        Realistic = 3
    }

    public enum Sampler {
        Random = 0,
        LowDiscrepancy = 1,
        Metropolis = 2,
        Erpt = 3
    }

    public enum Film {
        FlexImage = 0
    }

    public enum PixelFilter {
        Mitchell = 0,
        Box = 1,
        Triangle = 2,
        Gaussian = 3,
        Sinc = 4
    }

    public enum Accelerator {
        KdTree = 0,
        Qbvh = 1,
        Grid = 2,
        Unsafekdtree = 3,
        Bvh = 4,
        None = 5
    }

    public enum SurfaceIntegrator {
        BiDirectional = 0,
        Path = 1,
        ExPhotonMap = 2,
        DirectLighting = 3,
        DistributedPath = 4
    }

    public enum VolumeIntegrator {
        Emission = 0,
        Single = 1,
        Multi = 2
    }
}
