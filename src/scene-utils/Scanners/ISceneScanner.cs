﻿namespace Scene.Utils.Scanners {
    public interface ISceneScanner {
        Scene Scan(string scenePath);
        Scene Scan(string scenePath, string sceneContent);
    }
}
