﻿namespace Scene.Utils.Scanners {
    public static class Tokens {
        public const string Film                =   "Film";
        public const string Camera              =   "Camera";
        public const string Include             =   "Include";
        public const string Sampler             =   "Sampler";
        public const string Renderer            =   "Renderer";
        public const string Accelerator         =   "Accelerator";
        public const string PixelFilter         =   "PixelFilter";
        public const string VolumeIntegrator    =   "VolumeIntegrator";
        public const string SurfaceIntegrator   =   "SurfaceIntegrator";
        public const string MapName             =   "\"string mapname\"";
        public const string FileName            =   "\"string filename\"";
        public const string HaltSpp             =   "\"integer haltspp\"";
        public const string XResolution         =   "\"integer xresolution\"";
        public const string YResolution         =   "\"integer yresolution\"";
    }
}
