﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.IO;
using System.Linq;

namespace Scene.Utils.Scanners {
    public class SceneScanner : ISceneScanner {
        private readonly IFileHelper _fileHelper;

        private string _currentAttribute;

        public SceneScanner()
            : this(new FileHelper()) {
        }

        public SceneScanner(IFileHelper fileHelper) {
            Contract.Requires(fileHelper != null);

            _fileHelper = fileHelper;
            _currentAttribute = string.Empty;
        }

        public Scene Scan(string scenePath) {
            Contract.Requires(scenePath != null);

            string sceneContent = _fileHelper.GetFileContent(scenePath);

            return Scan(scenePath, sceneContent);
        }

        public Scene Scan(string scenePath, string sceneContent) {
            Contract.Requires(!string.IsNullOrEmpty(scenePath));
            Contract.Requires(sceneContent != null);

            var scene = new Scene {
                            Path = scenePath,
                            Dependencies = new List<string>()
                        };

            return Scan(scene, sceneContent);
        }

        private Scene Scan(Scene scene, string sceneContent)
        {
            TryScanResolution(scene, sceneContent);
            TryScanHaltSpp(scene, sceneContent);

            using (var reader = new StringReader(sceneContent)) {
                while (reader.Peek() != -1) {
                    var line = reader.ReadLine().Trim();

                    ScanLine(scene, line);
                }
            }

            return scene;
        }

        private void ScanLine(Scene scene, string line) {
            if (TryScanEmptyLine(line) ||
                TryScanComment(line)) {
                return;
            }

            // We want to keep track of the current Attribute we are under
            // so we treat the first "non-string" word of a line as a new
            // attribute:
            //  e.g.
            //      Let * represent the attribute token...
            // e.g.     *Film* "image"                  [good]
            //          0.01000 0.0111                  [number is not an attribute]
            //          "string filename" "foo.txt"     [parameter is not an attribute]
            //
            if (!line.StartsWith("\"")) {
                var tokens = line.Split(' ', '\t');
                var firstWord = tokens.FirstOrDefault() ?? string.Empty;
                double number = 0;
                if (!double.TryParse(firstWord, out number)) {
                    _currentAttribute = firstWord;
                }
            }

            if (TryScanIncludeDepedency(scene, line) ||
                TryScanFileNameDependency(scene, line) ||
                TryScanMapNameDependency(scene, line) ||
                TryScanFilm(scene, line) ||
                TryScanCamera(scene, line) ||
                TryScanSampler(scene, line) ||
                TryScanRenderer(scene, line) ||
                TryScanAccelerator(scene, line) ||
                TryScanPixelFilter(scene, line) ||
                TryScanVolumeIntegrator(scene, line) ||
                TryScanSurfaceintegrator(scene, line)) {
                return;
            }
        }

        private bool TryScanResolution(Scene scene, string sceneContent) {
            string xResText = null;
            string yResText = null;
            if (!TryScanToken(sceneContent, Tokens.XResolution, '[', ']', out xResText) ||
                !TryScanToken(sceneContent, Tokens.YResolution, '[', ']', out yResText)) {
                return false;
            }

            scene.XResolution = int.Parse(xResText);
            scene.YResolution = int.Parse(yResText);
            return true;
        }

        private bool TryScanHaltSpp(Scene scene, string sceneContent) {
            string haltsppText = null;
            if (!TryScanToken(sceneContent, Tokens.HaltSpp, '[', ']', out haltsppText)) {
                return false;
            }

            scene.HaltSamplesPerPixel = int.Parse(haltsppText);
            return true;
        }

        private bool TryScanSurfaceintegrator(Scene scene, string line) {
            return TryScanToken(
                        line,
                        Tokens.SurfaceIntegrator,
                        v => {
                            if (v == "directlighting") {
                                scene.SurfaceIntegrator = SurfaceIntegrator.DirectLighting;
                            }
                            else if (v == "distributedpath") {
                                scene.SurfaceIntegrator = SurfaceIntegrator.DistributedPath;
                            }
                            else if (v == "exphotonmap") {
                                scene.SurfaceIntegrator = SurfaceIntegrator.ExPhotonMap;
                            }
                            else if (v == "path") {
                                scene.SurfaceIntegrator = SurfaceIntegrator.Path;
                            }
                            else {
                                scene.SurfaceIntegrator = SurfaceIntegrator.BiDirectional;
                            }
                        });
        }

        private bool TryScanVolumeIntegrator(Scene scene, string line) {
            return TryScanToken(
                        line,
                        Tokens.VolumeIntegrator,
                        v => {
                            if (v == "multi") {
                                scene.VolumeIntegrator = VolumeIntegrator.Multi;
                            }
                            else if (v == "single") {
                                scene.VolumeIntegrator = VolumeIntegrator.Single;
                            }
                            else {
                                scene.VolumeIntegrator = VolumeIntegrator.Emission;
                            }
                        });
        }

        private bool TryScanPixelFilter(Scene scene, string line) {
            return TryScanToken(
                        line,
                        Tokens.PixelFilter,
                        v => {
                            if (v == "box") {
                                scene.PixelFilter = PixelFilter.Box;
                            }
                            else if (v == "gaussian") {
                                scene.PixelFilter = PixelFilter.Gaussian;
                            }
                            else if (v == "sinc") {
                                scene.PixelFilter = PixelFilter.Sinc;
                            }
                            else if (v == "triangle") {
                                scene.PixelFilter = PixelFilter.Triangle;
                            }
                            else {
                                scene.PixelFilter = PixelFilter.Mitchell;
                            }
                        });
        }

        private bool TryScanAccelerator(Scene scene, string line) {
            return TryScanToken(
                        line,
                        Tokens.Accelerator,
                        v => {
                            if (v == "none") {
                                scene.Accelerator = Accelerator.None;
                            }
                            else if (v == "qbvh") {
                                scene.Accelerator = Accelerator.Qbvh;
                            }
                            else if (v == "unsafekdtree") {
                                scene.Accelerator = Accelerator.Unsafekdtree;
                            }
                            else if (v == "grid") {
                                scene.Accelerator = Accelerator.Grid;
                            }
                            else if (v == "bvh") {
                                scene.Accelerator = Accelerator.Bvh;
                            }
                            else {
                                scene.Accelerator = Accelerator.KdTree;
                            }
                        });
        }

        private bool TryScanRenderer(Scene scene, string line) {
            return TryScanToken(
                        line,
                        Tokens.Renderer,
                        v => {
                            if (v == "hybridsampler") {
                                scene.Renderer = Renderer.HybridSampler;
                            }
                            else if (v == "hybridsppm") {
                                scene.Renderer = Renderer.HybridSppm;
                            }
                            else {
                                scene.Renderer = Renderer.Sampler;
                            }
                        });
        }

        private bool TryScanSampler(Scene scene, string line) {
            return TryScanToken(
                        line,
                        Tokens.Sampler,
                        v => {
                            if (v == "metropolis") {
                                scene.Sampler = Sampler.Metropolis;
                            }
                            else if (v == "erpt") {
                                scene.Sampler = Sampler.Erpt;
                            }
                            else if (v == "lowdiscrepancy") {
                                scene.Sampler = Sampler.LowDiscrepancy;
                            }
                            else {
                                scene.Sampler = Sampler.Random;
                            }
                        });
        }

        private bool TryScanCamera(Scene scene, string line) {
            return TryScanToken(
                        line,
                        Tokens.Camera,
                        v => {
                            if (v == "realistic") {
                                scene.Camera = Camera.Realistic;
                            }
                            else if (v == "environment") {
                                scene.Camera = Camera.Environment;
                            }
                            else if (v == "orthographic") {
                                scene.Camera = Camera.Orthographic;
                            }
                            else {
                                scene.Camera = Camera.Perspective;
                            }
                        });
        }

        private bool TryScanFilm(Scene scene, string line) {
            return TryScanToken(line, Tokens.Film, v => scene.Film = Film.FlexImage);
        }

        private bool TryScanFileNameDependency(Scene scene, string line) {
            return TryScanDependency(
                        scene,
                        line,
                        Tokens.FileName,
                        d => {
                            if (_currentAttribute == Tokens.Film) {
                                // The filename parameter under film specifies
                                // output name so this is _not_ a dependency.
                                scene.Dependencies.Remove(d);
                            }
                        });
        }

        private bool TryScanMapNameDependency(Scene scene, string line) {
            return TryScanDependency(scene, line, Tokens.MapName, d => { });
        }

        private bool TryScanIncludeDepedency(Scene scene, string line) {
            return TryScanDependency(
                        scene,
                        line,
                        Tokens.Include,
                        d => {
                            // If this dependency was included, then we need to scan that file as well
                            Scan(scene, _fileHelper.GetFileContent(d));
                        });
        }

        private bool TryScanDependency(Scene scene,
                                       string line,
                                       string dependencyToken,
                                       Action<string> onDependencyAdded) {
            return TryScanToken(
                    line,
                    dependencyToken,
                    dependency => {
                        // We want all depedencies to be listed as absolute paths.
                        if (!Path.IsPathRooted(dependency)) {
                            var sceneFolderPath = Path.GetDirectoryName(scene.Path);
                            dependency = Path.Combine(sceneFolderPath, dependency).Replace('\\', '/');
                        }

                        scene.Dependencies.Add(dependency);

                        onDependencyAdded(dependency);
                    });
        }

        private bool TryScanToken(string line, string token, Action<string> setter) {
            string value = null;
            if (!TryScanToken(line, token, '\"', '\"', out value)) {
                return false;
            }

            setter(value);
            return true;
        }

        private bool TryScanToken(string text,
                                  string token,
                                  char valueOpenToken,
                                  char valueCloseToken,
                                  out string value) {
            int tokenStartIndex = text.IndexOf(token);
            if (tokenStartIndex < 0) {
                value = null;
                return false;
            }

            int tokenEndIndex = tokenStartIndex + token.Length;
            int valueStartIndex = text.IndexOf(valueOpenToken, tokenEndIndex) + 1;
            if (valueStartIndex <= -1) {
                value = null;
                return false;
            }

            if (text.Length <= valueStartIndex) {
                value = null;
                return false;
            }

            int valueEndIndex = text.IndexOf(valueCloseToken, valueStartIndex + 1);
            if (valueEndIndex == -1) {
                value = null;
                return false;
            }


            value = text.Substring(valueStartIndex, (valueEndIndex - valueStartIndex));
            return true;
        }

        private bool TryScanComment(string line) {
            return line.Length > 0 && line[0] == '#';
        }

        private bool TryScanEmptyLine(string line) {
            return string.IsNullOrEmpty(line);
        }

        public interface IFileHelper {
            string GetFileContent(string path);
        }

        private class FileHelper : IFileHelper {
            public string GetFileContent(string path) {
                using (var fileReader = new StreamReader(path)) {
                    return fileReader.ReadToEnd();
                }
            }
        }
    }
}
