﻿using System;
using System.IO;
using Scene.Utils.Packagers;

namespace PbrtPack {
    class Program {
        static void Main(string[] args) {
            Mode mode;
            string argumentFilePath = null;
            string targetPath = null;
            if (!TryParseArgs(args, out mode, out argumentFilePath, out targetPath)) {
                PrintUsage();
                return;
            }

            var packager = new ScenePackager();

            switch (mode) {
                case Mode.Pack:
                    packager.Package(argumentFilePath, targetPath);
                    System.Console.WriteLine("Packed {0}", targetPath);
                    break;
                case Mode.Unpack:
                    packager.Unpackage(argumentFilePath, targetPath);
                    System.Console.WriteLine("Unpacked {0} to {1}",
                                             argumentFilePath,
                                             targetPath);
                    break;
                default:
                    throw new InvalidOperationException();
            }
        }

        private static bool TryParseArgs(string[] args,
                                         out Mode mode,
                                         out string argumentFilePath,
                                         out string outputTargetPath) {
            mode = default(Mode);
            argumentFilePath = null;
            outputTargetPath = null;

            if (args.Length < 2) {
                return false;
            }

            var filePath = Path.GetFullPath(args[1]);
            var targetPath = (args.Length > 2) ? Path.GetFullPath(args[2]) : null;
            if (args[0] == "--pack") {
                if (Path.GetExtension(filePath) != ".pbrt") {
                    System.Console.WriteLine("Invalid file - pack expects '.pbrt' files");
                    return false;
                }

                targetPath = targetPath ??
                             string.Format("{0}.pbrtpkg", Path.GetFileNameWithoutExtension(filePath));
                if (Path.GetExtension(targetPath) != ".pbrtpkg") {
                    System.Console.WriteLine("Invalid target - pack outputs '.pbrtpkg' files");
                    return false;
                }

                mode = Mode.Pack;
            }
            else if (args[0] == "--unpack") {
                if (Path.GetExtension(filePath) != ".pbrtpkg") {
                    System.Console.WriteLine("Invalid file - unpack expects '.pbrtpkg' files");
                    return false;
                }

                targetPath = targetPath ?? Directory.GetCurrentDirectory();
                mode = Mode.Unpack;
            }
            else {
                System.Console.WriteLine("Invalid argument \"{0}\"", args[0]);
                return false;
            }

            if (!File.Exists(filePath)) {
                System.Console.WriteLine("{0} does not exist", filePath);
                return false;
            }

            argumentFilePath = filePath;
            outputTargetPath = targetPath;

            return true;
        }

        private static void PrintUsage() {
            var usage =
@"usage:    pbrtpack --pack|--unpack [<args>]
                --pack <scene.pbrt> [<packageName>]
                --unpack <package.pbrtpkg> [<targetDirectory>]
";
            System.Console.WriteLine(usage);
        }

        private enum Mode {
            Pack,
            Unpack
        }
    }
}
