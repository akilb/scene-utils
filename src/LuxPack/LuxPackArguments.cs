﻿using Plossum.CommandLine;

namespace LuxPack {
    [CommandLineManager(ApplicationName = "luxpack",
                        Description = "Packages and unpackages .lxs files and their dependencies.",
                        Version = "0.1")]
    [CommandLineOptionGroup("ops",
                            Name = "Operations",
                            Require = OptionGroupRequirement.ExactlyOne)]
    [CommandLineOptionGroup("other",
                            Name = "Options",
                            Require = OptionGroupRequirement.All)]
    public class LuxPackArguments {
        [CommandLineOption(Name = "h",
                           Aliases = "help",
                           Description = "Show Help",
                           GroupId = "ops")]
        public bool Help = false;

        [CommandLineOption(Name = "pack",
                           Description = "Package .lxs scene and dependencies.",
                           GroupId = "ops")]
        public bool Pack { get; set; }

        [CommandLineOption(Name = "unpack",
                           Description = "Unpackage .lxspkg to target folder.",
                           GroupId = "ops")]
        public bool Unpack { get; set; }

        [CommandLineOption(Name = "source",
                           Description = "Scene/package to pack/unpack",
                           GroupId = "other")]
        public string Source { get; set; }

        [CommandLineOption(Name = "destination",
                           Description = "",
                           GroupId = "other")]
        public string Destination { get; set; }
    }
}