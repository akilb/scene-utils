﻿using System;
using Plossum.CommandLine;
using Scene.Utils.Packagers;

namespace LuxPack {
    class Program {
        static int Main(string[] args) {
            var options = new LuxPackArguments();
            var parser = new CommandLineParser(options);
            parser.Parse();

            if (options.Help) {
                Console.WriteLine(parser.UsageInfo.ToString(Console.BufferWidth, false));
                return 0;
            }
            else if (parser.HasErrors) {
                Console.WriteLine(parser.UsageInfo.ToString(Console.BufferWidth, true));
                return -1;
            }

            // TODO: Error checking for source and dest.

            var packager = new ScenePackager();
            if (options.Pack) {
                packager.Package(options.Source, options.Destination);
            }
            else {
                packager.Unpackage(options.Source, options.Destination);
            }

            return 0;
        }
    }
}
