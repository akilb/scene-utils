﻿using System.Collections.Generic;
using Ionic.Zip;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Scene.Utils.Packagers;
using Scene.Utils.Scanners;
using System.IO;

namespace Scene.Utils.Tests.Packagers {
    [TestClass]
    public class ScenePackagerTests {
        [TestMethod]
        public void Unpackage_ShouldExtractEveryEntry_InTheZip() {
            var targetDirectory = "/foo/bar/";
            var firstEntry = new ZipEntry();
            var secondEntry = new ZipEntry();
            var entries = new List<ZipEntry>() { firstEntry, secondEntry };
            var mockFactory = new Mock<ScenePackager.IZipFileFactory>(MockBehavior.Strict);
            var mockZip = new Mock<ScenePackager.IZipFile>(MockBehavior.Strict);
            mockFactory.Setup(f => f.ReadZip(It.IsAny<string>()))
                       .Returns(mockZip.Object);
            mockZip.Setup<IEnumerator<ZipEntry>>(z => z.GetEnumerator())
                   .Returns(entries.GetEnumerator());
            mockZip.Setup(z => z.Extract(firstEntry, targetDirectory, ExtractExistingFileAction.OverwriteSilently));
            mockZip.Setup(z => z.Extract(secondEntry, targetDirectory, ExtractExistingFileAction.OverwriteSilently));
            mockZip.Setup(z => z.Dispose());
            var packager = new ScenePackager(new Mock<ISceneScanner>(MockBehavior.Strict).Object,
                                             new Mock<ScenePackager.IFileHelper>(MockBehavior.Strict).Object,
                                             mockFactory.Object);

            packager.Unpackage("foo.pbrtpkg", targetDirectory);

            mockZip.VerifyAll();
        }

        [TestMethod]
        public void Package_ShouldAddSceneAndDependenciesToTheZipInAZipFolder() {
            var expectedArchiveFolder = "relative\\path";
            var dependencyName = "dep.txt";
            var expectedDependencyPath = Path.Combine("\\base", expectedArchiveFolder, dependencyName);
            var scene = new Scene {
                Path = "\\base\\foo.pbrt",
                Dependencies = new List<string> { expectedDependencyPath }
            };
            var mockScanner = new Mock<ISceneScanner>(MockBehavior.Strict);
            var mockFactory = new Mock<ScenePackager.IZipFileFactory>(MockBehavior.Strict);
            var mockZip = new Mock<ScenePackager.IZipFile>(MockBehavior.Strict);
            var mockFileHelper = new Mock<ScenePackager.IFileHelper>(MockBehavior.Strict);
            mockFileHelper.Setup(f => f.FileExists(It.IsAny<string>())).Returns(true);
            mockScanner.Setup(s => s.Scan(scene.Path))
                       .Returns(scene);
            mockFactory.Setup(f => f.CreateZip())
                       .Returns(mockZip.Object);
            mockZip.Setup(z => z.AddFile(expectedDependencyPath, expectedArchiveFolder))
                   .Callback(() => mockZip.Setup(z => z.AddFile(scene.Path, "")))
                   .Verifiable();
            mockZip.Setup(z => z.Save(It.IsAny<string>()));
            mockZip.Setup(z => z.Dispose());
            var packager = new ScenePackager(mockScanner.Object,
                                             mockFileHelper.Object,
                                             mockFactory.Object);

            packager.Package(scene.Path, "output.pbrtpkg");

            mockZip.Verify();
        }

        [TestMethod]
        public void Package_ShouldSaveZipWithGivenOutputName() {
            var expectedOutput = "output.pbrtpkg";
            var scene = new Scene {
                Path = "\\base\\foo.pbrt",
                Dependencies = new List<string>()
            };
            var mockScanner = new Mock<ISceneScanner>(MockBehavior.Strict);
            var mockFactory = new Mock<ScenePackager.IZipFileFactory>(MockBehavior.Strict);
            var mockZip = new Mock<ScenePackager.IZipFile>(MockBehavior.Strict);
            var mockFileHelper = new Mock<ScenePackager.IFileHelper>(MockBehavior.Strict);
            mockFileHelper.Setup(f => f.FileExists(It.IsAny<string>())).Returns(true);
            mockScanner.Setup(s => s.Scan(scene.Path))
                       .Returns(scene);
            mockFactory.Setup(f => f.CreateZip())
                       .Returns(mockZip.Object);
            mockZip.Setup(z => z.AddFile(It.IsAny<string>(), It.IsAny<string>()));
            mockZip.Setup(z => z.Save(expectedOutput)).Verifiable();
            mockZip.Setup(z => z.Dispose());
            var packager = new ScenePackager(mockScanner.Object,
                                             mockFileHelper.Object,
                                             mockFactory.Object);

            packager.Package(scene.Path, expectedOutput);

            mockZip.Verify();
        }
    }
}
