﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Scene.Utils.Scanners;
using Scene.Utils.Tests;

namespace Scene.Utils.Test.Scanners {
    [TestClass]
    public class SceneScannerTests {
        [TestMethod]
        public void Scanner_ShouldReturnAScene() {
            var scanner = new SceneScanner();

            Assert.IsNotNull(scanner.Scan("foo.pbrt", "#content"));
        }

        [TestMethod]
        public void Scanner_ShouldSetScenePath() {
            var expectedPath = "foo/bar/baz.pbrt";
            var scanner = new SceneScanner();

            var scene = scanner.Scan(expectedPath, string.Empty);

            Assert.AreEqual(expectedPath, scene.Path);
        }

        [TestMethod]
        public void Scanner_ShouldInitialiseSceneDependencies_EvenIfThereAreNoDependenciesDefined() {
            var scanner = new SceneScanner();

            var scene = scanner.Scan("foo.pbrt", "#no dependencies in this file");

            Assert.IsNotNull(scene.Dependencies);
        }

        [TestMethod]
        public void Scanner_ShouldAddDependencies_ToTheScene() {
            var expectedDependencies = new List<string> { "foo/bar.txt", "geometry/foo.dependency" };
            var content = string.Format("#some comment\n"               +
                                        "Include \"{0}\"\n"             +
                                        "\"string mapname\" [\"{1}\"]",
                                        expectedDependencies[0],
                                        expectedDependencies[1]);
            var mockFileHelper = new Mock<SceneScanner.IFileHelper>(MockBehavior.Strict);
            mockFileHelper.Setup(fh => fh.GetFileContent(It.IsAny<string>()))
                          .Returns(string.Empty);
            var scanner = new SceneScanner(mockFileHelper.Object);

            var scene = scanner.Scan("foo.pbrt", content);

            CollectionAssert.AreEqual(expectedDependencies, scene.Dependencies.ToList());
        }

        [TestMethod]
        public void Scanner_ShouldScanXResolution() {
            int expectedResolution = 363;
            var content = "Film \"image\"\n" +
                          "\"integer xresolution\"  [363]\n" +
                          "\"integer yresolution\"  [800]\n";
            var scanner = new SceneScanner();

            var scene = scanner.Scan("foo.pbrt", content);

            Assert.AreEqual(expectedResolution, scene.XResolution);
        }

        [TestMethod]
        public void Scanner_ShouldScanYResolution() {
            int expectedResolution = 800;
            var content = "Film \"image\"\n" +
                          "\"integer xresolution\"  [363]\n" +
                          "\"integer yresolution\"  [800]\n";
            var scanner = new SceneScanner();

            var scene = scanner.Scan("foo.pbrt", content);

            Assert.AreEqual(expectedResolution, scene.YResolution);
        }

        [TestMethod]
        public void Scanner_ShouldScanHaltSpp() {
            int expectedHaltSpp = 200;
            var content = "Film \"image\"\n" +
                          "\"integer xresolution\"  [363]\n" +
                          "\"integer yresolution\"  [800]\n" +
                          "\"integer haltspp\"      [200]\n";
            var scanner = new SceneScanner();

            var scene = scanner.Scan("foo.pbrt", content);

            Assert.AreEqual(expectedHaltSpp, scene.HaltSamplesPerPixel);
        }

        [TestMethod]
        public void Scanner_ShouldScanIncludedDependency() {
            int expectedHaltSpp = 200;
            string dependency = "./foo.lxo";
            var includedContent = string.Format("Film \"image\"\n" +
                                                "\"integer xresolution\"  [363]\n" +
                                                "\"integer yresolution\"  [800]\n" +
                                                "\"integer haltspp\"      [{0}]\n",
                                                expectedHaltSpp);
            var content = string.Format("Include \"{0}\"", dependency);
            var mockFileHelper = new Mock<SceneScanner.IFileHelper>(MockBehavior.Strict);
            mockFileHelper.Setup(fh => fh.GetFileContent(dependency))
                          .Returns(includedContent);
            var scanner = new SceneScanner(mockFileHelper.Object);

            var scene = scanner.Scan("foo.lxs", content);

            Assert.AreEqual(expectedHaltSpp, scene.HaltSamplesPerPixel);
        }

        private void VerifyFileNameDependency(string content,
                                              string dependencyPath,
                                              SceneScanner.IFileHelper fileHelper = null) {
            var sceneFolderPath = "/path/to/scene/";
            var scenePath = sceneFolderPath + "scene.lxs";
            var expectedDependency = Path.IsPathRooted(dependencyPath) ?
                                        dependencyPath :
                                        Path.Combine(sceneFolderPath, dependencyPath);

            var scanner = new SceneScanner(fileHelper ?? new Mock<SceneScanner.IFileHelper>(MockBehavior.Strict).Object);

            var scene = scanner.Scan(scenePath, content);

            Assert.AreEqual(expectedDependency, scene.Dependencies[0]);
        }

        [TestMethod]
        public void Scanner_ShouldScanFileNameDependency_RelativePath_FileNameOnly() {
            var dependencyPath = "dependency.txt";
            var content = string.Format("\"string filename\" [\"{0}\"]", dependencyPath);

            VerifyFileNameDependency(content, dependencyPath);
        }

        [TestMethod]
        public void Scanner_ShouldScanFileNameDependency_RelativePath_NoBraces() {
            var dependencyPath = "relative/path/dependency.txt";
            var content = string.Format("\"string filename\" \"{0}\"", dependencyPath);

            VerifyFileNameDependency(content, dependencyPath);
        }

        [TestMethod]
        public void Scanner_ShouldScanFileNameDependency_RelativePath() {
            var dependencyPath = "relative/path/dependency.txt";
            var content = string.Format("\"string filename\" [\"{0}\"]", dependencyPath);

            VerifyFileNameDependency(content, dependencyPath);
        }

        [TestMethod]
        public void Scanner_ShouldScanFileNameDependency_AbsolutePath() {
            var dependencyPath = "/absolute/path/dependency.txt";
            var content = string.Format("\"string filename\" [\"{0}\"]", dependencyPath);

            VerifyFileNameDependency(content, dependencyPath);
        }

        [TestMethod]
        public void Scanner_ShouldScanFileNameDependency_OnSameLineAsOtherStuff() {
            var dependencyPath = "/absolute/path/dependency.txt";
            var content = string.Format("SomeAttribute \"string filename\" [\"{0}\"]", dependencyPath);

            VerifyFileNameDependency(content, dependencyPath);
        }

        [TestMethod]
        public void Scanner_ShouldNotScanFileNameDependency_IfUnderFilmAttribute() {
            var dependencyPath = "/absolute/path/dependency.txt";
            var content = string.Format("Film \"image\"" +
                                        "\"string filename\" [\"{0}\"]",
                                        dependencyPath);
            var scanner = new SceneScanner();

            var scene = scanner.Scan("foo.txt", content);

            Assert.AreEqual(0, scene.Dependencies.Count);
        }

        [TestMethod]
        public void Scanner_ShouldScanMapNameDependency_RelativePath_FileNameOnly() {
            var dependencyPath = "dependency.txt";
            var content = string.Format("\"string mapname\" [\"{0}\"]", dependencyPath);

            VerifyFileNameDependency(content, dependencyPath);
        }

        [TestMethod]
        public void Scanner_ShouldScanMapNameDependency_RelativePath_NoBraces() {
            var dependencyPath = "relative/path/dependency.txt";
            var content = string.Format("\"string mapname\" \"{0}\"", dependencyPath);

            VerifyFileNameDependency(content, dependencyPath);
        }

        [TestMethod]
        public void Scanner_ShouldScanMapNameDependency_RelativePath() {
            var dependencyPath = "relative/path/dependency.txt";
            var content = string.Format("\"string mapname\" [\"{0}\"]", dependencyPath);

            VerifyFileNameDependency(content, dependencyPath);
        }

        [TestMethod]
        public void Scanner_ShouldScanMapNameDependency_AbsolutePath() {
            var dependencyPath = "/absolute/path/dependency.txt";
            var content = string.Format("\"string mapname\" [\"{0}\"]", dependencyPath);

            VerifyFileNameDependency(content, dependencyPath);
        }

        [TestMethod]
        public void Scanner_ShouldScanMapNameDependency_OnSameLineAsOtherStuff() {
            var dependencyPath = "/absolute/path/dependency.txt";
            var content = string.Format("SomeAttribute \"string mapname\" [\"{0}\"]", dependencyPath);

            VerifyFileNameDependency(content, dependencyPath);
        }

        [TestMethod]
        public void Scanner_ShouldScanIncludeDependency_RelativePath_FileNameOnly() {
            var dependencyPath = "dependency.txt";
            var content = string.Format("Include [\"{0}\"]", dependencyPath);
            var mockFileHelper = new Mock<SceneScanner.IFileHelper>(MockBehavior.Strict);
            mockFileHelper.Setup(f => f.GetFileContent(It.IsAny<string>())).Returns(string.Empty);

            VerifyFileNameDependency(content, dependencyPath, mockFileHelper.Object);
        }

        [TestMethod]
        public void Scanner_ShouldScanIncludeDependency_RelativePath_NoBraces() {
            var dependencyPath = "relative/path/dependency.txt";
            var content = string.Format("Include \"{0}\"", dependencyPath);
            var mockFileHelper = new Mock<SceneScanner.IFileHelper>(MockBehavior.Strict);
            mockFileHelper.Setup(f => f.GetFileContent(It.IsAny<string>())).Returns(string.Empty);

            VerifyFileNameDependency(content, dependencyPath, mockFileHelper.Object);
        }

        [TestMethod]
        public void Scanner_ShouldScanIncludeDependency_RelativePath() {
            var dependencyPath = "relative/path/dependency.txt";
            var content = string.Format("Include [\"{0}\"]", dependencyPath);
            var mockFileHelper = new Mock<SceneScanner.IFileHelper>(MockBehavior.Strict);
            mockFileHelper.Setup(f => f.GetFileContent(It.IsAny<string>())).Returns(string.Empty);

            VerifyFileNameDependency(content, dependencyPath, mockFileHelper.Object);
        }

        [TestMethod]
        public void Scanner_ShouldScanIncludeDependency_AbsolutePath() {
            var dependencyPath = "/absolute/path/dependency.txt";
            var content = string.Format("Include [\"{0}\"]", dependencyPath);
            var mockFileHelper = new Mock<SceneScanner.IFileHelper>(MockBehavior.Strict);
            mockFileHelper.Setup(f => f.GetFileContent(It.IsAny<string>())).Returns(string.Empty);

            VerifyFileNameDependency(content, dependencyPath, mockFileHelper.Object);
        }

        // TODO: Scan scenes where tokens are not on separate lines...

        [TestMethod]
        public void Scanner_SceneTest_SanMiguel() {
            var mockFileHelper = new Mock<SceneScanner.IFileHelper>(MockBehavior.Strict);
            mockFileHelper.Setup(h => h.GetFileContent(It.IsAny<string>()))
                          .Returns(string.Empty);
            var scanner = new SceneScanner(mockFileHelper.Object);

            var scene = scanner.Scan("sanmiguel_cam1.pbrt", SceneFiles.sanmiguel_cam1);

            // Just making sure things don't crash.
            // TODO: verify the scene attributes.
        }

        [TestMethod]
        public void Scanner_ShouldScan_FilmType_FlexImage() {
            var content = "Film \"fleximage\"  \"integer xresolution\" [1368] \"integer yresolution\" [1026]";
            var scanner = new SceneScanner();

            var scene = scanner.Scan("foo.pbrt", content);

            Assert.AreEqual(Film.FlexImage, scene.Film);
        }

        [TestMethod]
        public void Scanner_ShouldScan_FilmType_Default() {
            var content = "Film \"someotherfilmtype\"  \"integer xresolution\" [1368] \"integer yresolution\" [1026]";
            var scanner = new SceneScanner();

            var scene = scanner.Scan("foo.pbrt", content);

            Assert.AreEqual(Film.FlexImage, scene.Film);
        }

        [TestMethod]
        public void Scanner_ShouldScan_Renderer_Sampler() {
            var content = "Renderer \"sampler\"";
            var scanner = new SceneScanner();

            var scene = scanner.Scan("foo.pbrt", content);

            Assert.AreEqual(Renderer.Sampler, scene.Renderer);
        }

        [TestMethod]
        public void Scanner_ShouldScan_Renderer_HybridSampler() {
            var content = "Renderer \"hybridsampler\"";
            var scanner = new SceneScanner();

            var scene = scanner.Scan("foo.pbrt", content);

            Assert.AreEqual(Renderer.HybridSampler, scene.Renderer);
        }

        [TestMethod]
        public void Scanner_ShouldScan_Renderer_HybridSppm() {
            var content = "Renderer \"hybridsppm\"";
            var scanner = new SceneScanner();

            var scene = scanner.Scan("foo.pbrt", content);

            Assert.AreEqual(Renderer.HybridSppm, scene.Renderer);
        }

        [TestMethod]
        public void Scanner_ShouldScan_Renderer_Default() {
            var content = "Renderer \"somerenderer\"";
            var scanner = new SceneScanner();

            var scene = scanner.Scan("foo.pbrt", content);

            Assert.AreEqual(Renderer.Sampler, scene.Renderer);
        }

        [TestMethod]
        public void Scanner_ShouldScan_Camera_Perspective() {
            var content = "Camera \"perspective\"";
            var scanner = new SceneScanner();

            var scene = scanner.Scan("foo.pbrt", content);

            Assert.AreEqual(Camera.Perspective, scene.Camera);
        }

        [TestMethod]
        public void Scanner_ShouldScan_Camera_Environment() {
            var content = "Camera \"environment\"";
            var scanner = new SceneScanner();

            var scene = scanner.Scan("foo.pbrt", content);

            Assert.AreEqual(Camera.Environment, scene.Camera);
        }

        [TestMethod]
        public void Scanner_ShouldScan_Camera_Orthographic() {
            var content = "Camera \"orthographic\"";
            var scanner = new SceneScanner();

            var scene = scanner.Scan("foo.pbrt", content);

            Assert.AreEqual(Camera.Orthographic, scene.Camera);
        }

        [TestMethod]
        public void Scanner_ShouldScan_Camera_Realistic() {
            var content = "Camera \"realistic\"";
            var scanner = new SceneScanner();

            var scene = scanner.Scan("foo.pbrt", content);

            Assert.AreEqual(Camera.Realistic, scene.Camera);
        }

        [TestMethod]
        public void Scanner_ShouldScan_Camera_Default() {
            var content = "Camera \"somecamera\"";
            var scanner = new SceneScanner();

            var scene = scanner.Scan("foo.pbrt", content);

            Assert.AreEqual(Camera.Perspective, scene.Camera);
        }

        [TestMethod]
        public void Scanner_ShouldScan_Sampler_Metropolis() {
            var content = "Sampler \"metropolis\"";
            var scanner = new SceneScanner();

            var scene = scanner.Scan("foo.pbrt", content);

            Assert.AreEqual(Sampler.Metropolis, scene.Sampler);
        }

        [TestMethod]
        public void Scanner_ShouldScan_Sampler_LowDiscrepancy() {
            var content = "Sampler \"lowdiscrepancy\"";
            var scanner = new SceneScanner();

            var scene = scanner.Scan("foo.pbrt", content);

            Assert.AreEqual(Sampler.LowDiscrepancy, scene.Sampler);
        }

        [TestMethod]
        public void Scanner_ShouldScan_Sampler_Erpt() {
            var content = "Sampler \"erpt\"";
            var scanner = new SceneScanner();

            var scene = scanner.Scan("foo.pbrt", content);

            Assert.AreEqual(Sampler.Erpt, scene.Sampler);
        }

        [TestMethod]
        public void Scanner_ShouldScan_Sampler_Random() {
            var content = "Sampler \"random\"";
            var scanner = new SceneScanner();

            var scene = scanner.Scan("foo.pbrt", content);

            Assert.AreEqual(Sampler.Random, scene.Sampler);
        }

        [TestMethod]
        public void Scanner_ShouldScan_Sampler_Default() {
            var content = "Sampler \"somesampler\"";
            var scanner = new SceneScanner();

            var scene = scanner.Scan("foo.pbrt", content);

            Assert.AreEqual(Sampler.Random, scene.Sampler);
        }
    }
}
